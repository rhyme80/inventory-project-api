package main

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/graphql-go/handler"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"warehouse-project/database"
	"warehouse-project/graphql"
)

const defaultPort = "8080"

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	_, _, err := database.ConnectDB()
	if err != nil {
		return
	}

	schema := *graphql.GetSchema()

	h := handler.New(&handler.Config{
		Schema:     &schema,
		Pretty:     true,
		GraphiQL:   false,
		Playground: true,
	})

	http.Handle("/graphql", disableCors(h))
	http.HandleFunc("/files", UploadFile)
	log.Printf("connect to http://localhost:%s/ for GraphQL playground", port)
	log.Fatal(http.ListenAndServe(":"+port, nil))
}

func disableCors(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Authorization, Content-Type, Content-Length, Accept-Encoding")
		if r.Method == "OPTIONS" {
			w.Header().Set("Access-Control-Max-Age", "86400")
			w.WriteHeader(http.StatusOK)
			return
		}
		h.ServeHTTP(w, r)
	})
}

	func RespondWithError(err error, writer http.ResponseWriter) {
	writer.WriteHeader(http.StatusInternalServerError)
	json.NewEncoder(writer).Encode(err.Error())
}

func RespondWithSuccess(data interface{}, writer http.ResponseWriter) {
	writer.Header().Set("Content-Type", "application/json")
	writer.WriteHeader(http.StatusOK)
	json.NewEncoder(writer).Encode(data)
}

func UploadFile(w http.ResponseWriter, r *http.Request) {
	_ = r.ParseMultipartForm(1000)
	file, fileInfo, err := r.FormFile("file")
	if err != nil {
		RespondWithError(err, w)
		//log.Fatal(err)
	}
	fmt.Println(file)
	fmt.Println("---")
	fmt.Println(fileInfo)

	defer file.Close()

	uuid := uuid.New().String()
	nameFileTmp := strings.Replace(uuid, "-", "", -1)
	extensionFile := filepath.Ext(fileInfo.Filename)
	nameFile := "./tmp/files/" + nameFileTmp + extensionFile

	f, err := os.OpenFile(nameFile, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		RespondWithError(err, w)
		//log.Fatal(err)
	}

	defer f.Close()

	io.Copy(f, file)

	if err == nil {
		RespondWithSuccess(nameFile, w)
	} else {
		RespondWithError(err, w)
	}
}
