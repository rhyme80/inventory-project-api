package models

type TotalRegisters struct {
	TotalInput  float64 `json:"totalInput" bson:"totalInput"`
	TotalOutput float64 `json:"totalOutput" bson:"totalOutput"`
}
