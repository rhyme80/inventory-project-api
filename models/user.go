package models

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"warehouse-project/database"
)

type User struct {
	ID       string  `json:"id" bson:"_id"`
	Name     string `json:"name" bson:"name"`
	Lastname string `json:"lastname" bson:"lastname"`
	Phone    string `json:"phone" bson:"phone"`
	Address  string `json:"address" bson:"address"`
	Dni      string `json:"dni" bson:"dni"`
	Email    string `json:"email" bson:"email"`
	Username string `json:"username" bson:"username"`
	Password string `json:"password" bson:"password"`
}

func UserAddToBson(userI interface{}) (*primitive.ObjectID, *bson.M, error) {
	userM, ok := userI.(map[string]interface{})
	if !ok {
		return nil, nil, errors.New("Error casting user input ")
	}
	id := primitive.NewObjectID()
	userBson := bson.M{
		"_id": id,
	}
	_ = database.GetStringData(userM, userBson, "name")
	_ = database.GetStringData(userM, userBson, "lastname")
	_ = database.GetStringData(userM, userBson, "phone")
	_ = database.GetStringData(userM, userBson, "address")
	_ = database.GetStringData(userM, userBson, "dni")
	_ = database.GetStringData(userM, userBson, "email")
	_ = database.GetStringData(userM, userBson, "username")
	_ = database.GetStringData(userM, userBson, "password")
	return &id, &userBson, nil
}

func UserUpdateToBson(userI interface{}) (*bson.M, error) {
	userM, ok := userI.(map[string]interface{})
	if !ok {
		return nil, errors.New("Error casting user input ")
	}
	userBson := bson.M{}
	_ = database.GetStringData(userM, userBson, "name")
	_ = database.GetStringData(userM, userBson, "lastname")
	_ = database.GetStringData(userM, userBson, "phone")
	_ = database.GetStringData(userM, userBson, "address")
	_ = database.GetStringData(userM, userBson, "dni")
	_ = database.GetStringData(userM, userBson, "email")
	_ = database.GetStringData(userM, userBson, "username")
	_ = database.GetStringData(userM, userBson, "password")
	return &userBson, nil
}

func UserLoginBson(userI interface{}) (*bson.M, error) {
	userM, ok := userI.(map[string]interface{})
	if !ok {
		return nil, errors.New("Error casting user input ")
	}
	userBson := bson.M{}
	_ = database.GetStringData(userM, userBson, "username")
	_ = database.GetStringData(userM, userBson, "password")
	return &userBson, nil
}


type Claim struct {
	User `json:"user"`
	jwt.StandardClaims
}

type ResponseToken struct {
	Token string `json:"token"`
}
