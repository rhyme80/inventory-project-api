package models

import (
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"time"
	"warehouse-project/database"
)

type Register struct {
	ID       string    `json:"id" bson:"_id"`
	Date     time.Time `json:"date" bson:"date"`
	Article  *Article  `json:"article" bson:"article"`
	Cant     float64   `json:"cant" bson:"cant"`
	Type     string    `json:"type" bson:"type"`
	Locality string    `json:"locality" bson:"locality"`
}

func RegisterAddToBson(registerI interface{}) (*primitive.ObjectID, *bson.M, error) {
	registerM, ok := registerI.(map[string]interface{})
	if !ok {
		return nil, nil, errors.New("Error casting register input ")
	}
	id := primitive.NewObjectID()
	registerBson := bson.M{
		"_id":  id,
		"date": time.Now(),
	}
	_ = database.GetIdData(registerM, registerBson, "idArticle")
	_ = database.GetFloat64Data(registerM, registerBson, "cant")
	_ = database.GetStringData(registerM, registerBson, "type")
	_ = database.GetStringData(registerM, registerBson, "locality")
	return &id, &registerBson, nil
}

func RegisterUpdateToBson(registerI interface{}) (*bson.M, error) {
	registerM, ok := registerI.(map[string]interface{})
	if !ok {
		return nil, errors.New("Error casting register input ")
	}
	registerBson := bson.M{}
	_ = database.GetIdData(registerM, registerBson, "idArticle")
	_ = database.GetFloat64Data(registerM, registerBson, "cant")
	_ = database.GetStringData(registerM, registerBson, "type")
	_ = database.GetStringData(registerM, registerBson, "locality")
	return &registerBson, nil
}
