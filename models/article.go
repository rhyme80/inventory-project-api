package models

import (
	"errors"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"warehouse-project/database"
)

type Article struct {
	ID          string  `json:"id" bson:"_id"`
	Stock       float64 `json:"stock" bson:"stock"`
	Name        string  `json:"name" bson:"name"`
	UnitMeasure string  `json:"unitMeasure" bson:"unitMeasure"`
}

func ArticleAddToBson(articleI interface{}) (*primitive.ObjectID, *bson.M, error) {
	articleM, ok := articleI.(map[string]interface{})
	if !ok {
		return nil, nil, errors.New("Error casting article input ")
	}
	id := primitive.NewObjectID()
	articleBson := bson.M{
		"_id": id,
	}
	_ = database.GetFloat64Data(articleM, articleBson, "stock")
	_ = database.GetStringData(articleM, articleBson, "name")
	_ = database.GetStringData(articleM, articleBson, "unitMeasure")
	return &id, &articleBson, nil
}

func ArticleUpdateToBson(articleI interface{}) (*bson.M, error) {
	articleM, ok := articleI.(map[string]interface{})
	if !ok {
		return nil, errors.New("Error casting article input ")
	}
	articleBson := bson.M{}
	_ = database.GetFloat64Data(articleM, articleBson, "stock")
	_ = database.GetStringData(articleM, articleBson, "name")
	_ = database.GetStringData(articleM, articleBson, "unitMeasure")
	return &articleBson, nil
}
