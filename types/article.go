package types

import (
	"github.com/graphql-go/graphql"
)

var ArticleType = graphql.NewObject(graphql.ObjectConfig{
	Name:        "Article",
	Description: "type career",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: graphql.ID,
		},
		"stock": &graphql.Field{
			Type: graphql.Float,
		},
		"name": &graphql.Field{
			Type: graphql.String,
		},
		"unitMeasure": &graphql.Field{
			Type: graphql.String,
		},
	},
})

var ArticleInputType = graphql.NewInputObject(graphql.InputObjectConfig{
	Name: "ArticleInput",
	Fields: graphql.InputObjectConfigFieldMap{
		"name": &graphql.InputObjectFieldConfig{
			Type: graphql.NewNonNull(graphql.String),
		},
		"unitMeasure": &graphql.InputObjectFieldConfig{
			Type: graphql.NewNonNull(graphql.String),
		},
	},
})

var ArticleUpdateType = graphql.NewInputObject(graphql.InputObjectConfig{
	Name: "ArticleUpdate",
	Fields: graphql.InputObjectConfigFieldMap{
		"name": &graphql.InputObjectFieldConfig{
			Type: graphql.String,
		},
		"unitMeasure": &graphql.InputObjectFieldConfig{
			Type: graphql.String,
		},
	},
})
