package types

import (
	"github.com/graphql-go/graphql"
)

var RegisterType = graphql.NewObject(graphql.ObjectConfig{
	Name:        "Register",
	Description: "type career",
	Fields: graphql.Fields{
		"id": &graphql.Field{
			Type: graphql.ID,
		},
		"date": &graphql.Field{
			Type: graphql.DateTime,
		},
		"article": &graphql.Field{
			Type: ArticleType,
		},
		"cant": &graphql.Field{
			Type: graphql.Float,
		},
		"locality": &graphql.Field{
			Type: graphql.String,
		},
		"type": &graphql.Field{
			Type: graphql.String,
		},
	},
})

var RegisterInputType = graphql.NewInputObject(graphql.InputObjectConfig{
	Name: "RegisterInput",
	Fields: graphql.InputObjectConfigFieldMap{
		"idArticle": &graphql.InputObjectFieldConfig{
			Type: graphql.NewNonNull(graphql.ID),
		},
		"cant": &graphql.InputObjectFieldConfig{
			Type: graphql.NewNonNull(graphql.Float),
		},
		"locality": &graphql.InputObjectFieldConfig{
			Type: graphql.NewNonNull(graphql.String),
		},
		"type": &graphql.InputObjectFieldConfig{
			Type: graphql.NewNonNull(graphql.String),
		},
	},
})

var RegisterUpdateType = graphql.NewInputObject(graphql.InputObjectConfig{
	Name: "RegisterUpdate",
	Fields: graphql.InputObjectConfigFieldMap{
		"idArticle": &graphql.InputObjectFieldConfig{
			Type: graphql.ID,
		},
		"cant": &graphql.InputObjectFieldConfig{
			Type: graphql.Float,
		},
		"locality": &graphql.InputObjectFieldConfig{
			Type: graphql.String,
		},
		"type": &graphql.InputObjectFieldConfig{
			Type: graphql.String,
		},
	},
})
