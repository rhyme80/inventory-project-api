package services

import (
	"context"
	"errors"
	"fmt"
	"github.com/xuri/excelize/v2"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"strconv"
	"time"
	"warehouse-project/database"
	"warehouse-project/models"
)

const (
	CollectionRegister = "registers"
)

func AddRegister(registerInput bson.M, idRegister primitive.ObjectID) (*models.Register, error) {
	c, errC := database.GetCollection(CollectionRegister)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	_, errC = c.InsertOne(database.Ctx, registerInput)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	register, _ := GetRegister(idRegister.Hex())
	// add to article
	_ = CalcInputsOutput(register.Article.ID)
	return GetRegister(idRegister.Hex())
}

func UpdateRegister(idRegister string, registerInput bson.M) (*models.Register, error) {
	c, errC := database.GetCollection(CollectionRegister)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdRegister, _ := primitive.ObjectIDFromHex(idRegister)
	result, err := c.UpdateOne(database.Ctx,
		bson.M{"_id": objectIdRegister},
		bson.M{"$set": registerInput},
	)
	if err != nil {
		return nil, fmt.Errorf("Error 500. Couldn't update register %v ", err)
	}
	if result.MatchedCount != int64(1) {
		return nil, errors.New("Error 404. Holiday not found ")
	}
	return GetRegister(idRegister)
}

func RemoveRegister(idRegister string) (bool, error) {
	c, errC := database.GetCollection(CollectionRegister)
	if errC != nil {
		return false, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdRegister, _ := primitive.ObjectIDFromHex(idRegister)
	result, err := c.DeleteOne(database.Ctx, bson.M{"_id": objectIdRegister})
	if err != nil {
		return false, fmt.Errorf("Error 500. Couldn't delete register %v ", err)
	}
	if result.DeletedCount == 0 {
		return false, errors.New("Error 404. Register not found ")
	}
	register, _ := GetRegister(idRegister)
	_ = CalcInputsOutput(register.Article.ID)
	return true, nil
}

func GetRegister(idRegister string) (*models.Register, error) {
	c, errC := database.GetCollection(CollectionRegister)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdRegister, _ := primitive.ObjectIDFromHex(idRegister)
	var result models.Register
	pipeline := []bson.M{
		{
			"$match": bson.M{
				"_id": objectIdRegister,
			},
		},
		{
			"$lookup": bson.M{
				"from":         "articles",
				"localField":   "idArticle",
				"foreignField": "_id",
				"as":           "article",
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$article",
				"preserveNullAndEmptyArrays": true,
			},
		},
	}
	cursor, err := c.Aggregate(context.TODO(), pipeline)
	if err != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	for cursor.Next(context.TODO()) {
		if err = cursor.Decode(&result); err != nil {
			return nil, err
		}
	}
	return &result, nil
}

func GetRegisters() ([]*models.Register, error) {
	c, errC := database.GetCollection(CollectionRegister)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	var results []*models.Register
	pipeline := []bson.M{
		{
			"$lookup": bson.M{
				"from":         "articles",
				"localField":   "idArticle",
				"foreignField": "_id",
				"as":           "article",
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$article",
				"preserveNullAndEmptyArrays": true,
			},
		},
	}
	cursor, err := c.Aggregate(context.TODO(), pipeline)
	if err != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	err = cursor.All(context.TODO(), &results)
	if err != nil {
		return nil, err
	}
	return results, nil
}

func CalcInputsOutput(idArticle string) error {
	objectIdArticle, _ := primitive.ObjectIDFromHex(idArticle)
	totalRegister, err := TotalCantMov(objectIdArticle)
	if err != nil {
		return nil
	}
	_ = UpdateStockArticle(objectIdArticle, totalRegister.TotalInput-totalRegister.TotalOutput)
	return nil
}

func TotalCantMov(idArticle primitive.ObjectID) (*models.TotalRegisters, error) {
	c, errC := database.GetCollection(CollectionRegister)
	if errC != nil {
		return nil, errC
	}
	var result models.TotalRegisters
	pipeline := []bson.M{
		{
			"$match": bson.M{
				"$expr": bson.M{
					"$and": []bson.M{
						{
							"$eq": []interface{}{"$idArticle", idArticle},
						},
					},
				},
			},
		},
		{
			"$group": bson.M{
				"_id": nil,
				"totalInput": bson.M{
					"$sum": bson.M{
						"$cond": bson.M{
							"if":   bson.M{"$eq": []interface{}{"$type", "INPUT"}},
							"then": "$cant",
							"else": 0,
						},
					},
				},
				"totalOutput": bson.M{
					"$sum": bson.M{
						"$cond": bson.M{
							"if":   bson.M{"$eq": []interface{}{"$type", "OUTPUT"}},
							"then": "$cant",
							"else": 0,
						},
					},
				},
			},
		},
	}
	cursor, err := c.Aggregate(context.TODO(), pipeline)
	if err != nil {
		return nil, err
	}
	for cursor.Next(database.Ctx) {
		if err = cursor.Decode(&result); err != nil {
			return nil, err
		}
	}
	return &result, nil
}

func ReadExcel(path string) bool {
	f, err := excelize.OpenFile(path)
	if err != nil {
		fmt.Println(err)
		return false
	}
	defer func() {
		// Close the spreadsheet.
		f.Lock()
		//if err := f.Close(); err != nil {
		//	fmt.Println(err)
		//}
	}()

	// Get all the rows in the Sheet1.
	rows, err := f.GetRows("Sheet1")
	if err != nil {
		fmt.Println(err)
		return false
	}

	for i, row := range rows {
		if i == 0 {
			continue
		}
		article, _ := GetArticleByName(row[0])
		idRegister := primitive.NewObjectID()
		idArticle, _ := primitive.ObjectIDFromHex(article.ID)
		cant, _ := strconv.ParseFloat(row[2], 64)
		dateRegister, _ := time.Parse(row[4], "02-01-2006")
		var typeRegister string
		if row[1] == "ENTRADA" {
			typeRegister = "INPUT"
		} else if row[1] == "SALIDA" {
			typeRegister = "OUTPUT"
		}

		registerBson := bson.M{
			"_id":       idRegister,
			"date":      dateRegister,
			"cant":      cant,
			"type":      typeRegister,
			"locality":  row[3],
			"idArticle": idArticle,
		}
		_, err = AddRegister(registerBson, idRegister)
		if err != nil {
			return false
		}
	}

	return true
}
