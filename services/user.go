package services

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"golang.org/x/crypto/bcrypt"
	"warehouse-project/database"
	"warehouse-project/models"
)

const (
	CollectionUser = "users"
)

func AddUser(userInput bson.M, idUser primitive.ObjectID) (*models.User, error) {
	c, errC := database.GetCollection(CollectionUser)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}

	password := userInput["password"].(string)

	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}
	userInput["password"] = hashedPassword
	_, errC = c.InsertOne(database.Ctx, userInput)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	return GetUser(idUser.Hex())
}

func UpdateUser(idUser string, userInput bson.M) (*models.User, error) {
	c, errC := database.GetCollection(CollectionUser)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdUser, _ := primitive.ObjectIDFromHex(idUser)
	result, err := c.UpdateOne(database.Ctx,
		bson.M{"_id": objectIdUser},
		bson.M{"$set": userInput},
	)
	if err != nil {
		return nil, fmt.Errorf("Error 500. Couldn't update user %v ", err)
	}
	if result.MatchedCount != int64(1) {
		return nil, errors.New("Error 404. Holiday not found ")
	}
	return GetUser(idUser)
}

func RemoveUser(idUser string) (bool, error) {
	c, errC := database.GetCollection(CollectionUser)
	if errC != nil {
		return false, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdUser, _ := primitive.ObjectIDFromHex(idUser)
	result, err := c.DeleteOne(database.Ctx, bson.M{"_id": objectIdUser})
	if err != nil {
		return false, fmt.Errorf("Error 500. Couldn't delete user %v ", err)
	}
	if result.DeletedCount == 0 {
		return false, errors.New("Error 404. User not found ")
	}
	return true, nil
}

func GetUser(idUser string) (*models.User, error) {
	c, errC := database.GetCollection(CollectionUser)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdUser, _ := primitive.ObjectIDFromHex(idUser)
	var result models.User
	pipeline := []bson.M{
		{
			"$match": bson.M{
				"_id": objectIdUser,
			},
		},
	}
	cursor, err := c.Aggregate(context.TODO(), pipeline)
	if err != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	for cursor.Next(context.TODO()) {
		if err = cursor.Decode(&result); err != nil {
			return nil, err
		}
	}
	return &result, nil
}

func GetUsers() ([]*models.User, error) {
	c, errorC := database.GetCollection(CollectionUser)
	if errorC != nil {
		return nil, errorC
	}
	var results []*models.User
	cursor, err := c.Find(context.TODO(), bson.M{})
	if err != nil {
		return nil, err
	}
	err = cursor.All(context.TODO(), &results)
	return results, err
}

func UpdateStockUser(idUser primitive.ObjectID, stock float64) error {
	c, errC := database.GetCollection(CollectionUser)
	if errC != nil {
		return fmt.Errorf("Error 500. %v ", errC)
	}
	result, err := c.UpdateOne(database.Ctx,
		bson.M{"_id": idUser},
		bson.M{"$set": bson.M{
			"stock":     stock,
		}},
	)
	if err != nil {
		return fmt.Errorf("Error 500. Couldn't update bond %v ", err)
	}
	if result.MatchedCount != int64(1) {
		return errors.New("Error 404. career not found ")
	}
	return nil
}

func GetUserByUsername(username string) (*models.User, error) {
	c, errC := database.GetCollection(CollectionUser)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	var result models.User
	pipeline := []bson.M{
		{
			"$match": bson.M{
				"username": username,
			},
		},
	}
	cursor, err := c.Aggregate(context.TODO(), pipeline)
	if err != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	for cursor.Next(context.TODO()) {
		if err = cursor.Decode(&result); err != nil {
			return nil, err
		}
	}
	return &result, nil
}

//func ReadExcelUser(path string) bool {
//	f, err := excelize.OpenFile(path)
//	//f, err := excelize.OpenFile("./tmp/files/Book2.xlsx")
//	if err != nil {
//		fmt.Println(err)
//		return false
//	}
//	defer func() {
//		// Close the spreadsheet.
//		f.Lock()
//		//if err := f.Close(); err != nil {
//		//	fmt.Println(err)
//		//}
//	}()
//
//	// Get all the rows in the Sheet1.
//	rows, err := f.GetRows("Sheet1")
//	if err != nil {
//		fmt.Println(err)
//		return false
//	}
//
//	for i, row := range rows {
//		if i == 0 {
//			continue
//		}
//		warehouse, _ := GetWarehouseByName(row[4])
//		unitMeasure, _ := GetUnitMeasureByName(row[5])
//		category, _ := GetCategoryByName(row[6])
//		idUser := primitive.NewObjectID()
//
//		idWarehouse, _ := primitive.ObjectIDFromHex(warehouse.ID)
//		idUnitMeasure, _ := primitive.ObjectIDFromHex(unitMeasure.ID)
//		idCategory, _ := primitive.ObjectIDFromHex(category.ID)
//		dealerPrice, _ := strconv.ParseFloat(row[1], 64)
//		purchasePrice, _ := strconv.ParseFloat(row[2], 64)
//		sellingPrice, _ := strconv.ParseFloat(row[3], 64)
//		userBson := bson.M{
//			"_id":           idUser,
//			"description":   row[0],
//			"dealerPrice":   dealerPrice,
//			"purchasePrice": purchasePrice,
//			"sellingPrice":  sellingPrice,
//			"idWarehouse":   idWarehouse,
//			"idUnitMeasure": idUnitMeasure,
//			"idCategory":    idCategory,
//		}
//		_, err = AddUser(userBson, idUser)
//		if err != nil {
//			return false
//		}
//	}
//
//	return true
//}
