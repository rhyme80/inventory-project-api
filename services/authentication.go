package services

import (
	"crypto/rsa"
	"github.com/dgrijalva/jwt-go"
	"golang.org/x/crypto/bcrypt"
	"io/ioutil"
	"log"
	"time"
	"warehouse-project/models"
)

var (
	privateKey *rsa.PrivateKey
	publicKey  *rsa.PublicKey
)

func init() {
	privateBytes, err := ioutil.ReadFile("./keys/private.rsa")
	if err != nil {
		log.Fatal("Can't read private file")
	}

	publicBytes, err := ioutil.ReadFile("./keys/public.rsa.pub")
	if err != nil {
		log.Fatal("Can't read public file")
	}

	privateKey, err = jwt.ParseRSAPrivateKeyFromPEM(privateBytes)
	if err != nil {
		log.Fatal("Don't parse the private key")
	}
	publicKey, err = jwt.ParseRSAPublicKeyFromPEM(publicBytes)
	if err != nil {
		log.Fatal("Don't parse the public key")
	}
}

func GenerateJWT(user models.User) string {
	claims := models.Claim{
		User: user,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 1).Unix(),
			Issuer: "new token",
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodRS256, claims)
	result, err := token.SignedString(privateKey)
	if err != nil {
		log.Fatal("Don't could sign")
	}
	return result
}

func LoginUser(username, password string) (*models.ResponseToken, error) {
	var userModel models.User
	userModel.Username = username
	userModel.Password = password

	//userResponse, _ := GetUserByUsername(userInput["username"].(string))
	userResponse, _ := GetUserByUsername(userModel.Username)

	var result models.ResponseToken
	if userModel.Username == userResponse.Username {
		err := bcrypt.CompareHashAndPassword([]byte(userResponse.Password), []byte(userModel.Password))
		if err != nil {
			return nil, err
		}
		userModel.Password = ""
		token := GenerateJWT(userModel)
		result = models.ResponseToken{Token: token}
	}

	return &result, nil
}
