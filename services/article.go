package services

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"warehouse-project/database"
	"warehouse-project/models"
)

const (
	CollectionArticle = "articles"
)

func AddArticle(articleInput bson.M, idArticle primitive.ObjectID) (*models.Article, error) {
	c, errC := database.GetCollection(CollectionArticle)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	_, errC = c.InsertOne(database.Ctx, articleInput)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	return GetArticle(idArticle.Hex())
}

func UpdateArticle(idArticle string, articleInput bson.M) (*models.Article, error) {
	c, errC := database.GetCollection(CollectionArticle)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdArticle, _ := primitive.ObjectIDFromHex(idArticle)
	result, err := c.UpdateOne(database.Ctx,
		bson.M{"_id": objectIdArticle},
		bson.M{"$set": articleInput},
	)
	if err != nil {
		return nil, fmt.Errorf("Error 500. Couldn't update article %v ", err)
	}
	if result.MatchedCount != int64(1) {
		return nil, errors.New("Error 404. Holiday not found ")
	}
	return GetArticle(idArticle)
}

func RemoveArticle(idArticle string) (bool, error) {
	c, errC := database.GetCollection(CollectionArticle)
	if errC != nil {
		return false, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdArticle, _ := primitive.ObjectIDFromHex(idArticle)
	result, err := c.DeleteOne(database.Ctx, bson.M{"_id": objectIdArticle})
	if err != nil {
		return false, fmt.Errorf("Error 500. Couldn't delete article %v ", err)
	}
	if result.DeletedCount == 0 {
		return false, errors.New("Error 404. Article not found ")
	}
	return true, nil
}

func GetArticle(idArticle string) (*models.Article, error) {
	c, errC := database.GetCollection(CollectionArticle)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	objectIdArticle, _ := primitive.ObjectIDFromHex(idArticle)
	var result models.Article
	pipeline := []bson.M{
		{
			"$match": bson.M{
				"_id": objectIdArticle,
			},
		},
	}
	cursor, err := c.Aggregate(context.TODO(), pipeline)
	if err != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	for cursor.Next(context.TODO()) {
		if err = cursor.Decode(&result); err != nil {
			return nil, err
		}
	}
	return &result, nil
}

func GetArticles() ([]*models.Article, error) {
	c, errorC := database.GetCollection(CollectionArticle)
	if errorC != nil {
		return nil, errorC
	}
	var results []*models.Article
	cursor, err := c.Find(context.TODO(), bson.M{})
	if err != nil {
		return nil, err
	}
	err = cursor.All(context.TODO(), &results)
	return results, err
}

func UpdateStockArticle(idArticle primitive.ObjectID, stock float64) error {
	c, errC := database.GetCollection(CollectionArticle)
	if errC != nil {
		return fmt.Errorf("Error 500. %v ", errC)
	}
	result, err := c.UpdateOne(database.Ctx,
		bson.M{"_id": idArticle},
		bson.M{"$set": bson.M{
			"stock":     stock,
		}},
	)
	if err != nil {
		return fmt.Errorf("Error 500. Couldn't update bond %v ", err)
	}
	if result.MatchedCount != int64(1) {
		return errors.New("Error 404. career not found ")
	}
	return nil
}

func GetArticleByName(name string) (*models.Article, error) {
	c, errC := database.GetCollection(CollectionArticle)
	if errC != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	var result models.Article
	pipeline := []bson.M{
		{
			"$match": bson.M{
				"name": name,
			},
		},
	}
	cursor, err := c.Aggregate(context.TODO(), pipeline)
	if err != nil {
		return nil, fmt.Errorf("Error 500. %v ", errC)
	}
	for cursor.Next(context.TODO()) {
		if err = cursor.Decode(&result); err != nil {
			return nil, err
		}
	}
	return &result, nil
}

//func ReadExcelArticle(path string) bool {
//	f, err := excelize.OpenFile(path)
//	//f, err := excelize.OpenFile("./tmp/files/Book2.xlsx")
//	if err != nil {
//		fmt.Println(err)
//		return false
//	}
//	defer func() {
//		// Close the spreadsheet.
//		f.Lock()
//		//if err := f.Close(); err != nil {
//		//	fmt.Println(err)
//		//}
//	}()
//
//	// Get all the rows in the Sheet1.
//	rows, err := f.GetRows("Sheet1")
//	if err != nil {
//		fmt.Println(err)
//		return false
//	}
//
//	for i, row := range rows {
//		if i == 0 {
//			continue
//		}
//		warehouse, _ := GetWarehouseByName(row[4])
//		unitMeasure, _ := GetUnitMeasureByName(row[5])
//		category, _ := GetCategoryByName(row[6])
//		idArticle := primitive.NewObjectID()
//
//		idWarehouse, _ := primitive.ObjectIDFromHex(warehouse.ID)
//		idUnitMeasure, _ := primitive.ObjectIDFromHex(unitMeasure.ID)
//		idCategory, _ := primitive.ObjectIDFromHex(category.ID)
//		dealerPrice, _ := strconv.ParseFloat(row[1], 64)
//		purchasePrice, _ := strconv.ParseFloat(row[2], 64)
//		sellingPrice, _ := strconv.ParseFloat(row[3], 64)
//		articleBson := bson.M{
//			"_id":           idArticle,
//			"description":   row[0],
//			"dealerPrice":   dealerPrice,
//			"purchasePrice": purchasePrice,
//			"sellingPrice":  sellingPrice,
//			"idWarehouse":   idWarehouse,
//			"idUnitMeasure": idUnitMeasure,
//			"idCategory":    idCategory,
//		}
//		_, err = AddArticle(articleBson, idArticle)
//		if err != nil {
//			return false
//		}
//	}
//
//	return true
//}
