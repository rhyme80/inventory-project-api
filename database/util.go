package database

import (
	"errors"
	"math/rand"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"

	//"reflect"
	"testing"
	"time"
)

type NotFoundError struct {
	Name string
}

func (e *NotFoundError) Error() string { return e.Name + ": not found" }

func GetStringDataDefNull(data map[string]interface{}, bsonData bson.M, field string) error {
	temp, ok := data[field].(string)
	if ok {
		bsonData[field] = temp
		return nil
	} else {
		bsonData[field] = nil
		return errors.New("Not found " + field)
	}
}

/* function get String Data */
func GetStringData(data map[string]interface{}, bsonData bson.M, field string) error {
	temp, ok := data[field].(string)
	if ok {
		bsonData[field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetStringDataL2(data map[string]interface{}, bsonData bson.M, model, field string) error {
	temp, ok := data[field].(string)
	if ok {
		bsonData[model+".$."+field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetStringDataL2S(data map[string]interface{}, bsonData bson.M, model, field string) error {
	temp, ok := data[field].(string)
	if ok {
		bsonData[model+"."+field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetStringDataL3AA(data map[string]interface{}, bsonData bson.M, model, model2, filter1, filter2, field string) error {
	temp, ok := data[field].(string)
	if ok {
		bsonData[model+".$["+filter1+"]."+model2+".$["+filter2+"]."+field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetStringDataL3OA(data map[string]interface{}, bsonData bson.M, model, model2, filter2, field string) error {
	temp, ok := data[field].(string)
	if ok {
		bsonData[model+"."+model2+".$["+filter2+"]."+field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

/* function get String Data */
func GetStringDataNotEmpty(data map[string]interface{}, bsonData bson.M, field string) error {
	temp, ok := data[field].(string)
	if ok && temp != "" {
		bsonData[field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetArrayStringData(data map[string]interface{}, bsonData bson.M, field string) error {
	temp, ok := data[field].([]interface{})
	if ok {
		aux := make([]string, 0)
		//aux := []string{}
		for _, v := range temp {
			if st, ok := v.(string); ok {
				aux = append(aux, st)
			}
		}
		bsonData[field] = aux
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}
func GetArrayStringDataL2(data map[string]interface{}, bsonData bson.M, fieldToSave string, field string) error {
	temp, ok := data[field].([]interface{})
	if ok {
		aux := make([]string, 0)
		//aux := []string{}
		for _, v := range temp {
			if st, ok := v.(string); ok {
				aux = append(aux, st)
			}
		}
		bsonData[fieldToSave+".$."+field] = aux
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}
/* function get Float64 Data */
func GetFloat64Data(data map[string]interface{}, bsonData bson.M, field string) error {
	temp, ok := data[field].(float64)
	if ok {
		bsonData[field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetFloat64DataL2S(data map[string]interface{}, bsonData bson.M, fieldToSave string, field string) error {
	temp, ok := data[field].(float64)
	if ok {
		bsonData[fieldToSave+"."+field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetFloat64DataL2(data map[string]interface{}, bsonData bson.M, fieldToSave string, field string) error {
	temp, ok := data[field].(float64)
	if ok {
		bsonData[fieldToSave+".$."+field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetArrayFloatDataL2(data map[string]interface{}, bsonData bson.M, fieldToSave string, field string) error {
	temp, ok := data[field].([]interface{})
	if ok {
		aux := make([]float64, 0)
		//aux := []float64{}
		for _, v := range temp {
			if st, ok := v.(float64); ok {
				aux = append(aux, st)
			}
		}
		bsonData[fieldToSave+".$."+field] = aux
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetFloat64DataL3AA(data map[string]interface{}, bsonData bson.M, model, model2, filter1, filter2, field string) error {
	temp, ok := data[field].(float64)
	if ok {
		bsonData[model+".$["+filter1+"]."+model2+".$["+filter2+"]."+field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetFloat64DataL3OA(data map[string]interface{}, bsonData bson.M, model, model2, filter2, field string) error {
	temp, ok := data[field].(float64)
	if ok {
		bsonData[model+"."+model2+".$["+filter2+"]."+field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetFloat64DataL3AO(data map[string]interface{}, bsonData bson.M, model1, model2, filter1, field string) error {
	temp, ok := data[field].(float64)
	if ok {
		bsonData[model1+".$["+filter1+"]."+model2+"."+field] = temp
		return nil
	} else {
		return errors.New("Not found" + field)
	}
}

func GetArrayFloatData(data map[string]interface{}, bsonData bson.M, field string) error {
	temp, ok := data[field].([]interface{})
	if ok {
		aux := make([]float64, 0)
		//aux := []float64{}
		for _, v := range temp {
			if st, ok := v.(float64); ok {
				aux = append(aux, st)
			}
		}
		bsonData[field] = aux
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetIntData(data map[string]interface{}, bsonData bson.M, field string) error {
	temp, ok := data[field].(int)
	if ok {
		bsonData[field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetIntDataL2(data map[string]interface{}, bsonData bson.M, model, field string) error {
	temp, ok := data[field].(int)
	if ok {
		bsonData[model+".$."+field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetIntDataL2S(data map[string]interface{}, bsonData bson.M, model, field string) error {
	temp, ok := data[field].(int)
	if ok {
		bsonData[model+"."+field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetIntDataL3AA(data map[string]interface{}, bsonData bson.M, model, model2, filter1, filter2, field string) error {
	temp, ok := data[field].(int)
	if ok {
		bsonData[model+".$["+filter1+"]."+model2+".$["+filter2+"]."+field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}
func GetIntDataL3AO(data map[string]interface{}, bsonData bson.M, model1, model2, filter1, field string) error {
	temp, ok := data[field].(int)
	if ok {
		bsonData[model1+".$["+filter1+"]."+model2+"."+field] = temp
		return nil
	} else {
		return errors.New("Not found" + field)
	}
}
func GetIntDataL4AAA(data map[string]interface{}, bsonData bson.M, model, model2,model3, filter1, filter2,filter3, field string) error {
	temp, ok := data[field].(int)
	if ok {
		bsonData[model+".$["+filter1+"]."+model2+".$["+filter2+"]."+model3+".$["+filter3+"]."+field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetIntDataPositive(data map[string]interface{}, bsonData bson.M, field string) error {
	temp, ok := data[field].(int)
	if ok {
		if temp < 0 {
			return errors.New("error, " + field)
		}
		bsonData[field] = temp
		return nil
	}
	return nil
}

func GetTimeData(data map[string]interface{}, bsonData bson.M, field string) error {
	temp, ok := data[field].(string)
	if ok {
		timeStp, err := time.Parse(time.RFC3339, temp)
		if err != nil {
			return err
		}
		bsonData[field] = timeStp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetTimeDataL2(data map[string]interface{}, bsonData bson.M, model string, field string) error {
	temp, ok := data[field].(string)
	if ok {
		timeStp, err := time.Parse(time.RFC3339, temp)
		if err != nil {
			return err
		}
		bsonData[model+".$."+field] = timeStp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetTimeDataL2S(data map[string]interface{}, bsonData bson.M, model string, field string) error {
	temp, ok := data[field].(string)
	if ok {
		timeStp, err := time.Parse(time.RFC3339, temp)
		if err != nil {
			return err
		}
		bsonData[model+"."+field] = timeStp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetTimeDataL3AA(data map[string]interface{}, bsonData bson.M, model, model2, filter1, filter2 string, field string) error {
	temp, ok := data[field].(string)
	if ok {
		timeStp, err := time.Parse(time.RFC3339, temp)
		if err != nil {
			return err
		}
		bsonData[model+".$["+filter1+"]."+model2+".$["+filter2+"]."+field] = timeStp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetTimeDataL3OA(data map[string]interface{}, bsonData bson.M, model, model2, filter string, field string) error {
	temp, ok := data[field].(string)
	if ok {
		timeStp, err := time.Parse(time.RFC3339, temp)
		if err != nil {
			return err
		}
		bsonData[model+"."+model2+".$["+filter+"]."+field] = timeStp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

/* function get Boolean Data */
func GetBooleanData(data map[string]interface{}, bsonData bson.M, field string) error {
	temp, ok := data[field].(bool)
	if ok {
		bsonData[field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

/* function get Boolean Data */
func GetBooleanDataL2(data map[string]interface{}, bsonData bson.M, model string, field string) error {
	temp, ok := data[field].(bool)
	if ok {
		bsonData[model+".$."+field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

/* function get Boolean Data */
func GetBooleanDataL2S(data map[string]interface{}, bsonData bson.M, model string, field string) error {
	temp, ok := data[field].(bool)
	if ok {
		bsonData[model+"."+field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}
func GetBooleanDataL3AA(data map[string]interface{}, bsonData bson.M, model, model1, filter1, filter2, field string) error {
	temp, ok := data[field].(bool)
	if ok {
		bsonData[model+".$["+filter1+"]."+model1+".$["+filter2+"]."+field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}
func GetBooleanDataL4AA(data map[string]interface{}, bsonData bson.M, model, model2,model3, filter1, filter2,filter3, field string) error {
	temp, ok := data[field].(bool)
	if ok {
		bsonData[model+".$["+filter1+"]."+model2+".$["+filter2+"]."+model3+".$["+filter3+"]."+field] = temp
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}
func GetIdData(data map[string]interface{}, bsonData bson.M, field string) error {
	temp, ok := data[field].(string)
	if ok {
		id, err := primitive.ObjectIDFromHex(temp)
		if err != nil {
			return err
		}
		bsonData[field] = id
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetIdDataL2(data map[string]interface{}, bsonData bson.M, model string, field string) error {
	temp, ok := data[field].(string)
	if ok {
		id, err := primitive.ObjectIDFromHex(temp)
		if err != nil {
			return err
		}
		bsonData[model+".$."+field] = id
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetIdDataL3AA(data map[string]interface{}, bsonData bson.M, model, model2, filter1, filter2, field string) error {
	temp, ok := data[field].(string)
	if ok {
		id, err := primitive.ObjectIDFromHex(temp)
		if err != nil {
			return err
		}
		bsonData[model+".$["+filter1+"]."+model2+".$["+filter2+"]."+field] = id
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

// update with objectid or with null value
func GetIdDataOrNull(data map[string]interface{}, bsonData bson.M, field string) error {
	temp, ok := data[field].(string)
	if ok {
		id, err := primitive.ObjectIDFromHex(temp)
		if err != nil {
			return err
		}
		bsonData[field] = id
		return nil
	} else {
		bsonData[field] = nil
		return nil
	}
}

func GetIdDataR(data map[string]interface{}, bsonData bson.M, field string) (*primitive.ObjectID, error) {
	temp, ok := data[field].(string)
	if ok {
		id, err := primitive.ObjectIDFromHex(temp)
		if err != nil {
			return nil, err
		}
		bsonData[field] = id
		return &id, nil
	} else {
		return nil, errors.New("Not found " + field)
	}
}

func GetIdDataUpdate(data map[string]interface{}, bsonData bson.M, field string) error {
	temp, ok := data[field].(string)
	if ok {
		id, err := primitive.ObjectIDFromHex(temp)
		if err != nil {
			return err
		}
		bsonData[field] = id
		return nil
	} else {
		return nil
	}
}

func GetIdsFromInterface(data interface{}) (*[]primitive.ObjectID, error) {
	var ids []primitive.ObjectID
	temp, ok := data.([]string)
	if !ok {
		return nil, errors.New("Error casting ids ")
	} else {
		for _, id := range temp {
			idr, err := primitive.ObjectIDFromHex(id)
			if err != nil {
				return nil, err
			} else {
				ids = append(ids, idr)
			}
		}
		return &ids, nil
	}
}

func GetIdsFromInterfaceV2(data interface{}) ([]primitive.ObjectID, error) {
	ids := make([]primitive.ObjectID, 0)
	temp, ok := data.([]interface{})
	if !ok || len(temp) == 0 {
		return nil, errors.New("Error casting ids ")
	} else {
		for _, id := range temp {
			idA, err := primitive.ObjectIDFromHex(id.(string))
			if err != nil {
				return nil, errors.New("Error decoding id ")
			}
			ids = append(ids, idA)
		}
		return ids, nil
	}
}

func GetTimeFromInterface(data interface{}) (*time.Time, error) {
	temp, ok := data.(string)
	if ok {
		tm, err := time.Parse(time.RFC3339, temp)
		if err != nil {
			return nil, err
		}
		return &tm, nil
	}
	return nil, errors.New("Error. Could not cast time ")
}

// function for get Media
func GetMediaData(data map[string]interface{}, bsonData bson.M, field string, idUser *primitive.ObjectID) error {
	temp, ok := data[field].(map[string]interface{})
	if ok {
		bsonMediaData := bson.M{
			"date":           time.Now(),
			"idUserUploaded": &idUser,
		}
		_ = GetStringData(temp, bsonMediaData, "url")
		_ = GetStringData(temp, bsonMediaData, "name")
		_ = GetFloat64Data(temp, bsonMediaData, "size")
		bsonData[field] = bsonMediaData
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetMediaDataL2(data map[string]interface{}, bsonData bson.M, model, field string, idUser *primitive.ObjectID) error {
	temp, ok := data[field].(map[string]interface{})
	if ok {
		bsonMediaData := bson.M{
			"date":           time.Now(),
			"idUserUploaded": &idUser,
		}
		_ = GetStringData(temp, bsonMediaData, "url")
		_ = GetStringData(temp, bsonMediaData, "name")
		_ = GetFloat64Data(temp, bsonMediaData, "size")
		bsonData[model+".$."+field] = bsonMediaData
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetMediaDataL2S(data map[string]interface{}, bsonData bson.M, model, field string, idUser *primitive.ObjectID) error {
	temp, ok := data[field].(map[string]interface{})
	if ok {
		bsonMediaData := bson.M{
			"date": time.Now(),
			"idUserUploaded": idUser,
		}
		_ = GetStringData(temp, bsonMediaData, "url")
		_ = GetStringData(temp, bsonMediaData, "name")
		_ = GetFloat64Data(temp, bsonMediaData, "size")
		bsonData[model+"."+field] = bsonMediaData
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetMediaDataL3AA(data map[string]interface{}, bsonData bson.M, model, model2, filter1, filter2, field string, idUser primitive.ObjectID) error {
	temp, ok := data[field].(map[string]interface{})
	if ok {
		bsonMediaData := bson.M{
			"date":           time.Now(),
			"idUserUploaded": idUser,
		}
		_ = GetStringData(temp, bsonMediaData, "url")
		_ = GetFloat64Data(temp, bsonMediaData, "size")
		bsonData[model+".$["+filter1+"]."+model2+".$["+filter2+"]."+field] = bsonMediaData
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

func GetMediaDataL3OA(data map[string]interface{}, bsonData bson.M, model, model2, filter, field string, idUser *primitive.ObjectID) error {
	temp, ok := data[field].(map[string]interface{})
	if ok {
		bsonMediaData := bson.M{
			"date":           time.Now(),
			"idUserUploaded": idUser,
		}
		_ = GetStringData(temp, bsonMediaData, "url")
		_ = GetStringData(temp, bsonMediaData, "name")
		_ = GetFloat64Data(temp, bsonMediaData, "size")
		bsonData[model+"."+model2+".$["+filter+"]."+field] = bsonMediaData
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

/* function for setup test case */
func SetupTestCase(t *testing.T) func(t *testing.T) {
	t.Log("setup test case")
	return func(t *testing.T) {
		t.Log("teardown test case")
	}
}

/* function for setup Sub test */
func SetupSubTest(t *testing.T) func(t *testing.T) {
	t.Log("setup sub test")
	return func(t *testing.T) {
		t.Log("teardown sub test")
	}
}

/*Function for get root*/
func GetFloatMatrix(data map[string]interface{}, data2Set bson.M, field string) error {
	matrixData, ok := data[field].([][]float64)
	if ok {
		data2Set[field] = matrixData
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}

/*Function for get root*/
func GetListData(data map[string]interface{}, data2Set bson.M, field string) error {
	_, ok := data[field]
	if ok {
		//var List []primitive.ObjectID
		////for i := 0; i < list; i++ {
		////	List = append(List, bson.ObjectIdHex())
		////}
		//data2Set[field] = List
		return nil
	} else {

		return errors.New("Not found " + field)
	}
}

//function to get existence of element in array
func ItemExists(slice []primitive.ObjectID, val primitive.ObjectID) (int, bool) {
	for i, item := range slice {
		if item == val {
			return i, true
		}
	}
	return -1, false
}

func RemoveObjectIdInArray(s []primitive.ObjectID, i int) []primitive.ObjectID {
	s[i] = s[len(s)-1]
	return s[:len(s)-1]
}

/*
 IncWeekend
*/
func IncWeekend(t time.Time) int {
	//time in UTC
	t = t.UTC()
	//switch weekday of t
	switch t.Weekday() {
	case time.Friday:
		//if friday return 3 days until monday
		h, _, _ := t.Clock()
		if h >= 12+10 {
			return 3
		}
	case time.Saturday:
		//if saturday return 2 days
		return 2
	case time.Sunday:
		//if sunday return 1 day
		h, m, _ := t.Clock()
		if h < 12+10 {
			return 1
		}
		if h == 12+10 && m <= 5 {
			return 1
		}
	}
	return 0
}

/*
 IsWeekend verify if time is weekend
 * @param t time.Time
@return bool
*/
func IsWeekend(t time.Time) bool {
	t = t.UTC()
	switch t.Weekday() {
	case time.Friday:
		h, _, _ := t.Clock()
		if h >= 12+10 {
			return true
		}
	case time.Saturday:
		return true
	case time.Sunday:
		h, m, _ := t.Clock()
		if h < 12+10 {
			return true
		}
		if h == 12+10 && m <= 5 {
			return true
		}
	}
	return false
}

var DOCU_STATES = []string{"TO_WORK", "FINISH"}
var PCI_STATES = []string{"TO_RES", "FINISH"}

/* test structure*/
type CaseTest struct {
	Name         string
	Id           primitive.ObjectID
	Idaux        primitive.ObjectID
	NAccess      int
	Order        int
	Position     string
	Nam          string
	Des          string
	Url          string
	Icon         string
	Dni          string
	Period       string
	Address      string
	Days         []string
	Start        time.Time
	End          time.Time
	Model        bson.M
	IdList       []primitive.ObjectID
	ObjectList   []interface{}
	ThereIsError bool
	Action       bool
}

func GenerateRandomPassword(length int) string {
	rand.Seed(time.Now().UnixNano())
	chars := []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
		"0123456789" + "!@#$%&*/-")
	var b strings.Builder
	for i := 0; i < length; i++ {
		b.WriteRune(chars[rand.Intn(len(chars))])
	}
	return b.String()
}

func ConvertMonthS2I(month string) int {
	var number int
	switch month {
	case "jan":
		number = 1
	case "feb":
		number = 2
	case "mar":
		number = 3
	case "apr":
		number = 4
	case "may":
		number = 5
	case "jun":
		number = 6
	case "jul":
		number = 7
	case "aug":
		number = 8
	case "sep":
		number = 9
	case "oct":
		number = 10
	case "nov":
		number = 11
	case "dec":
		number = 12
	}
	return number
}

func ConvertMonthI2S(month int) string {
	var stringMonth string
	switch month {
	case 1:
		stringMonth = "jan"
	case 2:
		stringMonth = "feb"
	case 3:
		stringMonth = "mar"
	case 4:
		stringMonth = "apr"
	case 5:
		stringMonth = "may"
	case 6:
		stringMonth = "jun"
	case 7:
		stringMonth = "jul"
	case 8:
		stringMonth = "aug"
	case 9:
		stringMonth = "sep"
	case 10:
		stringMonth = "oct"
	case 11:
		stringMonth = "nov"
	case 12:
		stringMonth = "dec"
	}
	return stringMonth
}

func GetGeoPointData(data map[string]interface{}, bsonData bson.M, field string) error {
	temp, ok := data[field].(map[string]interface{})
	if ok {
		bsonMediaData := bson.M{
			"type": "Point",
		}
		_ = GetArrayFloatData(temp, bsonMediaData, "coordinates")
		bsonData[field] = bsonMediaData
		return nil
	} else {
		return errors.New("Not found " + field)
	}
}