package queries

import (
	"github.com/graphql-go/graphql"
	"warehouse-project/services"
	"warehouse-project/types"
)

var UserQuery = &graphql.Field{
	Description: "Get article by id",
	Type:        types.UserType,
	Args: graphql.FieldConfigArgument{
		"idUser": &graphql.ArgumentConfig{
			Type:        graphql.NewNonNull(graphql.String),
			Description: "ID of article",
		},
	},
	Resolve: func(p graphql.ResolveParams) (interface{}, error) {
		idUser := p.Args["idUser"].(string)
		article, err := services.GetUser(idUser)
		if err != nil {
			return nil, err
		}
		return *article, nil
	},
}

var UsersQuery = &graphql.Field{
	Description: "Get all articles ",
	Type:        graphql.NewList(types.UserType),
	Resolve: func(p graphql.ResolveParams) (interface{}, error) {
		articles, err := services.GetUsers()
		if err != nil {
			return nil, err
		}
		return articles, nil
	},
}

var LoginQuery = &graphql.Field{
	Type: graphql.String,
	Args: graphql.FieldConfigArgument{
		"username": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(graphql.String),
		},
		"password": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(graphql.String),
		},
	},
	Resolve: func(p graphql.ResolveParams) (interface{}, error) {
		username := p.Args["username"].(string)
		password := p.Args["password"].(string)
		token, err := services.LoginUser(username, password)
		if err != nil {
			return nil, err
		}
		return *token, nil
	},
}
