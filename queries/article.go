package queries

import (
	"github.com/graphql-go/graphql"
	"warehouse-project/services"
	"warehouse-project/types"
)

var ArticleQuery = &graphql.Field{
	Description: "Get article by id",
	Type:        types.ArticleType,
	Args: graphql.FieldConfigArgument{
		"idArticle": &graphql.ArgumentConfig{
			Type:        graphql.NewNonNull(graphql.String),
			Description: "ID of article",
		},
	},
	Resolve: func(p graphql.ResolveParams) (interface{}, error) {
		idArticle := p.Args["idArticle"].(string)
		article, err := services.GetArticle(idArticle)
		if err != nil {
			return nil, err
		}
		return *article, nil
	},
}

var ArticlesQuery = &graphql.Field{
	Description: "Get all articles ",
	Type:        graphql.NewList(types.ArticleType),
	Resolve: func(p graphql.ResolveParams) (interface{}, error) {
		articles, err := services.GetArticles()
		if err != nil {
			return nil, err
		}
		return articles, nil
	},
}
