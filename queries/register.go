package queries

import (
	"github.com/graphql-go/graphql"
	"warehouse-project/services"
	"warehouse-project/types"
)

var RegisterQuery = &graphql.Field{
	Description: "Get register by id",
	Type:        types.RegisterType,
	Args: graphql.FieldConfigArgument{
		"idRegister": &graphql.ArgumentConfig{
			Type:        graphql.NewNonNull(graphql.String),
			Description: "ID of register",
		},
	},
	Resolve: func(p graphql.ResolveParams) (interface{}, error) {
		idRegister := p.Args["idRegister"].(string)
		register, err := services.GetRegister(idRegister)
		if err != nil {
			return nil, err
		}
		return *register, nil
	},
}

var RegistersQuery = &graphql.Field{
	Description: "Get all registers ",
	Type:        graphql.NewList(types.RegisterType),
	Resolve: func(p graphql.ResolveParams) (interface{}, error) {
		registers, err := services.GetRegisters()
		if err != nil {
			return nil, err
		}
		return registers, nil
	},
}
