package mutations

import (
	"github.com/graphql-go/graphql"
	"warehouse-project/models"
	"warehouse-project/services"
	"warehouse-project/types"
)

var AddRegisterMutation = &graphql.Field{
	Type:        types.RegisterType,
	Description: "Add Register",
	Args: graphql.FieldConfigArgument{
		"register": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(types.RegisterInputType),
		},
	},
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		registerM := params.Args["register"]
		idRegister, registerBson, err := models.RegisterAddToBson(registerM)
		if err != nil {
			return nil, err
		}
		register, err := services.AddRegister(*registerBson, *idRegister)
		if err != nil {
			return nil, err
		}
		return *register, nil
	},
}

var UpdateRegisterMutation = &graphql.Field{
	Type:        types.RegisterType,
	Description: "Update Register",
	Args: graphql.FieldConfigArgument{
		"idRegister": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(graphql.String),
		},
		"register": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(types.RegisterUpdateType),
		},
	},
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		idRegister := params.Args["idRegister"].(string)
		registerBson, err := models.RegisterUpdateToBson(params.Args["register"])
		if err != nil {
			return nil, err
		}
		register, err := services.UpdateRegister(idRegister, *registerBson)
		if err != nil {
			return nil, err
		}
		return *register, nil
	},
}

var RemoveRegisterMutation = &graphql.Field{
	Type:        graphql.Boolean,
	Description: "Remove Register",
	Args: graphql.FieldConfigArgument{
		"idRegister": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(graphql.String),
		},
	},
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		idRegister := params.Args["idRegister"].(string)
		register, err := services.RemoveRegister(idRegister)
		if err != nil {
			return nil, err
		}
		return register, nil
	},
}

var ImportExcelMutation = &graphql.Field{
	Type:        graphql.Boolean,
	Description: "Decode Registers from XLSX file",
	Args: graphql.FieldConfigArgument{
		"path": &graphql.ArgumentConfig{
			Type:        graphql.NewNonNull(graphql.String),
			Description: "path to file in server",
		},
	},
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		path := params.Args["path"].(string)

		return services.ReadExcel(path), nil
	},
}
