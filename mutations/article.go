package mutations

import (
	"github.com/graphql-go/graphql"
	"warehouse-project/models"
	"warehouse-project/services"
	"warehouse-project/types"
)

var AddArticleMutation = &graphql.Field{
	Type:        types.ArticleType,
	Description: "Add Article",
	Args: graphql.FieldConfigArgument{
		"article": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(types.ArticleInputType),
		},
	},
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		articleM := params.Args["article"]
		idArticle, articleBson, err := models.ArticleAddToBson(articleM)
		if err != nil {
			return nil, err
		}
		article, err := services.AddArticle(*articleBson, *idArticle)
		if err != nil {
			return nil, err
		}
		return *article, nil
	},
}

var UpdateArticleMutation = &graphql.Field{
	Type:        types.ArticleType,
	Description: "Update Article",
	Args: graphql.FieldConfigArgument{
		"idArticle": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(graphql.String),
		},
		"article": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(types.ArticleUpdateType),
		},
	},
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		idArticle := params.Args["idArticle"].(string)
		articleBson, err := models.ArticleUpdateToBson(params.Args["article"])
		if err != nil {
			return nil, err
		}
		article, err := services.UpdateArticle(idArticle, *articleBson)
		if err != nil {
			return nil, err
		}
		return *article, nil
	},
}

var RemoveArticleMutation = &graphql.Field{
	Type:        graphql.Boolean,
	Description: "Remove Article",
	Args: graphql.FieldConfigArgument{
		"idArticle": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(graphql.String),
		},
	},
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		idArticle := params.Args["idArticle"].(string)
		article, err := services.RemoveArticle(idArticle)
		if err != nil {
			return nil, err
		}
		return article, nil
	},
}

//var ImportExcelArticleMutation = &graphql.Field{
//	Type:        graphql.Boolean,
//	Description: "Decode Registers from XLSX file",
//	Args: graphql.FieldConfigArgument{
//		"path": &graphql.ArgumentConfig{
//			Type:        graphql.NewNonNull(graphql.String),
//			Description: "path to file in server",
//		},
//	},
//	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
//		path := params.Args["path"].(string)
//
//		return services.ReadExcelArticle(path), nil
//	},
//}
