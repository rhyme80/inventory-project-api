package mutations

import (
	"github.com/graphql-go/graphql"
	"warehouse-project/models"
	"warehouse-project/services"
	"warehouse-project/types"
)

var AddUserMutation = &graphql.Field{
	Type:        types.UserType,
	Description: "Add User",
	Args: graphql.FieldConfigArgument{
		"user": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(types.UserInputType),
		},
	},
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		userM := params.Args["user"]
		idUser, userBson, err := models.UserAddToBson(userM)
		if err != nil {
			return nil, err
		}
		user, err := services.AddUser(*userBson, *idUser)
		if err != nil {
			return nil, err
		}
		return *user, nil
	},
}

var UpdateUserMutation = &graphql.Field{
	Type:        types.UserType,
	Description: "Update User",
	Args: graphql.FieldConfigArgument{
		"idUser": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(graphql.String),
		},
		"user": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(types.UserUpdateType),
		},
	},
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		idUser := params.Args["idUser"].(string)
		userBson, err := models.UserUpdateToBson(params.Args["user"])
		if err != nil {
			return nil, err
		}
		user, err := services.UpdateUser(idUser, *userBson)
		if err != nil {
			return nil, err
		}
		return *user, nil
	},
}

var RemoveUserMutation = &graphql.Field{
	Type:        graphql.Boolean,
	Description: "Remove User",
	Args: graphql.FieldConfigArgument{
		"idUser": &graphql.ArgumentConfig{
			Type: graphql.NewNonNull(graphql.String),
		},
	},
	Resolve: func(params graphql.ResolveParams) (interface{}, error) {
		idUser := params.Args["idUser"].(string)
		user, err := services.RemoveUser(idUser)
		if err != nil {
			return nil, err
		}
		return user, nil
	},
}
