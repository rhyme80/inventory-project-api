package graphql

import (

	//-- SCHOLAR
	inventoryMutations "warehouse-project/mutations"
	inventoryQueries "warehouse-project/queries"

	"github.com/graphql-go/graphql"
)

var rootQuery = graphql.NewObject(graphql.ObjectConfig{
	Name: "RootQuery",
	Fields: graphql.Fields{
		// INVENTORY
		"getArticles": inventoryQueries.ArticlesQuery,
		"getArticle":  inventoryQueries.ArticleQuery,

		"getRegisters": inventoryQueries.RegistersQuery,
		"getRegister":  inventoryQueries.RegisterQuery,

		"getUsers": inventoryQueries.UsersQuery,
		"getUser":  inventoryQueries.UserQuery,
		"login":    inventoryQueries.LoginQuery,
	},
})

// root mutation
var rootMutation = graphql.NewObject(graphql.ObjectConfig{
	Name: "RootMutation",
	Fields: graphql.Fields{
		// Articles
		"addArticle":    inventoryMutations.AddArticleMutation,
		"updateArticle": inventoryMutations.UpdateArticleMutation,
		"removeArticle": inventoryMutations.RemoveArticleMutation,

		// Registers
		"addRegister":         inventoryMutations.AddRegisterMutation,
		"updateRegister":      inventoryMutations.UpdateRegisterMutation,
		"removeRegister":      inventoryMutations.RemoveRegisterMutation,
		"importExcelRegister": inventoryMutations.ImportExcelMutation,

		// Users
		"addUser":    inventoryMutations.AddUserMutation,
		"updateUser": inventoryMutations.UpdateUserMutation,
		"removeUser": inventoryMutations.RemoveUserMutation,
	},
})

func GetSchema() *graphql.Schema {

	var schema, _ = graphql.NewSchema(graphql.SchemaConfig{
		Query:    rootQuery,
		Mutation: rootMutation,
	})
	return &schema
}
